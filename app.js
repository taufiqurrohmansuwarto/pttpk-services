// other docs
// https://hapi.dev/module/boom/#status

// fucking wrap with docker

require("dotenv").config();
const express = require("express");
const app = express();

// for the fucking handling errors;
const boom = require("express-boom-v2");
app.use(boom());

// routing
app.use("/v1", require("./routes/v1"));
app.use("/v2", require("./routes/v2"));

// except all routes
app.use(async (req, res) => {
  res.boom.notFound();
});

const port = process.env.PORT;

app.listen(port, "127.0.0.1", () => {
  console.log(`Server started on port ${port}`);
});
