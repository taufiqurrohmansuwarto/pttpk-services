const knex = require("../knex");
const { Model } = require("objection");

Model.knex(knex);

class Users extends Model {
  static get tableName() {
    return "users";
  }

  static get modifiers() {
    return {
      simple(builder) {
        builder.select(
          "username",
          "nama_lengkap",
          "email",
          "no_telp",
          "users.id_skpd",
          "level",
          "blokir"
        );
      },
    };
  }

  static get jsonSchema() {
    return {
      type: "object",
      required: ["username", "password", "nama_lengkap"],
      properties: {
        username: {
          type: "string",
        },
        password: {
          type: "string",
        },
      },
    };
  }

  static get idColumn() {
    return "username";
  }

  static get relationMappings() {
    const skpd = require("./skpd.model");

    return {
      skpd: {
        modelClass: skpd,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "users.id_skpd",
          to: "skpd.id",
        },
      },
    };
  }
}

module.exports = Users;
