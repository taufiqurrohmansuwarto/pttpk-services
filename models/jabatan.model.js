const { Model } = require("objection");
const knex = require("../knex");

Model.knex(knex);

class Jabatan extends Model {
  static get tableName() {
    return "jabatan";
  }

  static get idColumn() {
    return "id_jabatan";
  }

  static get modifiers() {
    return {
      simple(builder) {
        builder.select("id_jabatan", "name");
      },
    };
  }
}

module.exports = Jabatan;
