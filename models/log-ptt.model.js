const { Model } = require("objection");
const knex = require("../knex");

Model.knex(knex);

class LogPtt extends Model {
  static get tableName() {
    return "log_ptt";
  }

  static get idColumn() {
    return "log_id";
  }
}

module.exports = LogPtt;
