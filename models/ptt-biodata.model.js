const knex = require("../knex");
const { Model, raw } = require("objection");
const PttPendidikan = require("./ptt-pendidikan.model");
const SettingAgama = require("./setting-agama.model");
const SettingStatusKawin = require("./setting-status-kawin.model");

Model.knex(knex);

class PttBiodata extends Model {
  static get tableName() {
    return "ptt_biodata";
  }

  static get modifiers() {
    return {
      oidc(builder) {
        builder.select(
          "ptt_biodata.id_ptt",
          "nama",
          "foto",
          "niptt",
          "jk",
          "thn_lahir"
        );
      },
      minimize(builder) {
        builder.select(
          "ptt_biodata.id_ptt",
          "niptt",
          "nama",
          "foto",
          "niptt",
          "jk",
          raw(`TIMESTAMPDIFF(YEAR, thn_lahir, CURDATE())`).as("usia"),
          raw(
            `(select name from skpd where id = left(ptt_biodata.id_skpd, 3))`
          ).as("nama_skpd")
        );
      },
      simple(builder) {
        builder.select(
          "ptt_biodata.id_ptt",
          "nama",
          "foto",
          "niptt",
          "nik",
          "tempat_lahir",
          "thn_lahir",
          "jk",
          "kode_pos",
          "email",
          "alamat",
          "id_skpd",
          "id_agama",
          "id_kawin"
        );
      },
    };
  }

  static get idColumn() {
    return "id_ptt";
  }

  static get relationMappings() {
    const skpd = require("./skpd.model");
    const jabatan = require("./ptt-jabatan.model");

    return {
      statusKawin: {
        modelClass: SettingStatusKawin,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "ptt_biodata.id_kawin",
          to: "setting_status_kawin.id_kawin",
        },
      },
      agama: {
        modelClass: SettingAgama,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "ptt_biodata.id_agama",
          to: "setting_agama.id_agama",
        },
      },
      pendidikan: {
        modelClass: PttPendidikan,
        relation: Model.HasManyRelation,
        join: {
          from: "ptt_pendidikan.id_ptt",
          to: "ptt_biodata.id_ptt",
        },
      },
      jabatan: {
        modelClass: jabatan,
        relation: Model.HasManyRelation,
        join: {
          from: "ptt_biodata.id_ptt",
          to: "ptt_jabatan.id_ptt",
        },
      },
      skpd: {
        modelClass: skpd,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "ptt_biodata.id_skpd",
          to: "skpd.id",
        },
      },
    };
  }
}

module.exports = PttBiodata;
