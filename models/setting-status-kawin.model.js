const knex = require("../knex");
const { Model } = require("objection");

Model.knex(knex);

class SettingStatusKawin extends Model {
  static get tableName() {
    return "setting_status_kawin";
  }

  static get modifiers() {}

  static get idColumn() {
    return "kawin_id";
  }
}

module.exports = SettingStatusKawin;
