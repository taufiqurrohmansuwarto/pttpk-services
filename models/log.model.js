const knex = require("../knex");
const { Model } = require("objection");

Model.knex(knex);

class Log extends Model {
  static get tableName() {
    return "log";
  }

  static get idColumn() {
    return "id";
  }
}

module.exports = Log;
