const knex = require("../knex");
const { Model } = require("objection");

Model.knex(knex);

class RwytSuamiIstri extends Model {
  static get tableName() {
    return "rwyt_suami_istri";
  }

  static get relationMappings() {
    const RefPekerjaan = require("./ref-pekerjaan.model");
    const refStatusSuamiIstri = require("./ref-status-suami-istri.model");
    const rwytAnak = require("./rwyt-anak.model");

    return {
      pekerjaan: {
        modelClass: RefPekerjaan,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "rwyt_suami_istri.pekerjaan_id",
          to: "ref_pekerjaan.pekerjaan_id",
        },
      },
      statusSuamiIstri: {
        modelClass: refStatusSuamiIstri,
        relation: Model.BelongsToOneRelation,
        join: {
          from: "rwyt_suami_istri.status_suami_istri_id",
          to: "ref_status_suami_istri.status_suami_istri_id",
        },
      },
      anak: {
        modelClass: rwytAnak,
        relation: Model.HasManyRelation,
        join: {
          from: "rwyt_suami_istri.suami_istri_id",
          to: "rwyt_anak.suami_istri_id",
        },
      },
    };
  }

  static get idColumn() {
    return "suami_istri_id";
  }
}

module.exports = RwytSuamiIstri;
