const { Model } = require("objection");
const knex = require("../knex");

Model.knex(knex);

class Jenjang extends Model {
  static get tableName() {
    return "jenjang";
  }

  static get idColumn() {
    return "id_jenjang";
  }
}

module.exports = Jenjang;
