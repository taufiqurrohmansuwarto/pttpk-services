const knex = require("../knex");
const { Model } = require("objection");

Model.knex(knex);

class RefJurusan extends Model {
  static get tableName() {
    return "ref_jurusan";
  }

  static get modifiers() {
    return {
      aktif(builder) {
        builder.where("ptt_jabatan.aktif", "Y");
      },
    };
  }

  static get idColumn() {
    return "ref_jurusan_id";
  }
}

module.exports = RefJurusan;
