const model = require("../models/ptt-biodata.model");

// get the detail information employee
const detail = async (req, res) => {
  const { id } = req.params;
  const result = await model
    .query()
    .findById(id)
    .modify("simple")
    .withGraphFetched("[agama, statusKawin]");
  res.json(result);
};

// update
const update = async (req, res) => {};

module.exports = { detail, update };
