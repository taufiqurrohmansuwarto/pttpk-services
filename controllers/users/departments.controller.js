const skpdModel = require("../../models/skpd.model");
const userModel = require("../../models/users.model");
const arrayToTree = require("array-to-tree");

const list = async (req, res) => {
  const { username } = req.params;
  //   this should be middleware check if the username exists
  const user = await userModel.query().where({ username }).first();

  if (user) {
    const result = await skpdModel
      .query()
      .where("id", "like", `${user?.id_skpd}%`);

    let data;
    if (req.query?.type === "tree") {
      data = arrayToTree(result, {
        customID: "id",
        parentProperty: "pId",
      });
    } else {
      data = result;
    }
    res.json({ data });
  } else {
    res.boom.notFound();
  }
};
const detail = async (req, res) => {};
const create = async (req, res) => {};
const update = async (req, res) => {};
const remove = async (req, res) => {};

module.exports = {
  list,
  detail,
  create,
  update,
  remove,
};
