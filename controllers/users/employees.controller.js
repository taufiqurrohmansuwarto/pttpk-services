const userModel = require("../../models/users.model");
const employeeModel = require("../../models/ptt-biodata.model");
const detailSkpd = require("../../utils/detail-skpd");
const { raw } = require("objection");

const employee = async (req, res) => {
  try {
    const { id } = req.query;
    if (id) {
      const currentId = id?.split(",");
      const result = await employeeModel
        .query()
        .whereIn("id_ptt", currentId)
        .select("id_ptt", "nama", "niptt", "id_skpd");

      let data;
      if (result.length) {
        let promise = [];
        const uniqSkpd = detailSkpd.findUnique(result);
        uniqSkpd.forEach((x) => promise.push(detailSkpd.detail(x)));
        const hasil = await Promise.all(promise);
        data = result?.map((r) => {
          const detailSkpd = hasil.find((x) => x.id === r?.id_skpd);
          return {
            ...r,
            perangkat_daerah: detailSkpd?.detail,
          };
        });
      }
      res.json({ data });
    } else {
      res.json({ data: [] });
    }
  } catch (error) {
    res.boom.notFound("error");
  }
};

const list = async (req, res) => {
  try {
    const { username } = req.params;
    const currentUser = await userModel.query().where({ username }).first();

    if (currentUser) {
      const result = await employeeModel
        .query()
        .where("id_skpd", "like", `${currentUser?.id_skpd}%`)
        // .andWhere("blokir", "N")
        .andWhere("aktif", "Y")
        .select(
          "id_ptt",
          "niptt",
          "id_skpd",
          "nama",
          "nik",
          raw("DATE_FORMAT(??,'%d-%m-%Y')", "thn_lahir").as("thn_lahir"),
          "jk"
        )
        .orderBy("id_skpd", "asc")
        .orderBy("id_ptt", "asc");

      let data;

      if (result.length) {
        let promise = [];
        const uniqSkpd = detailSkpd.findUnique(result);
        uniqSkpd.forEach((x) => promise.push(detailSkpd.detail(x)));
        const hasil = await Promise.all(promise);
        data = result?.map((r) => {
          const detailSkpd = hasil.find((x) => x.id === r?.id_skpd);
          return {
            ...r,
            perangkat_daerah: detailSkpd?.detail,
          };
        });
      }
      res.json({ data });
    } else {
      res.boom.notFound();
    }
  } catch (error) {
    console.log(error);
    res.boom.notFound();
  }
};

const detail = async (req, res) => {};
const create = async (req, res) => {};
const update = async (req, res) => {};
const remove = async (req, res) => {};

module.exports = {
  list,
  detail,
  create,
  update,
  remove,
  employee,
};
