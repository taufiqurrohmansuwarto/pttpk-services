const index = async (req, res) => {};
const detail = async (req, res) => {};
const create = async (req, res) => {};
const update = async (req, res) => {};
const patch = async (req, res) => {};
const remove = async (req, res) => {};

module.exports = {
  index,
  detail,
  create,
  update,
  patch,
  remove,
};
