const model = require("../models/jenjang.model");

const list = async (req, res) => {
  try {
    const result = await model.query();
    res.json({ data: result });
  } catch (error) {}
};

const detail = async (req, res) => {};
const patch = async (req, res) => {};
const remove = async (req, res) => {};
const create = async (req, res) => {};

module.exports = {
  list,
  detail,
  patch,
  remove,
  create,
};
