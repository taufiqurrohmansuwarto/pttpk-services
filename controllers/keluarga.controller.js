const riwayatSuamiIstriModel = require("../models/rwyt-suami-istri.model");

const index = async (req, res) => {
  // its kind userId
  const { id } = req.params;
  const data = await riwayatSuamiIstriModel
    .query()
    .where("id_ptt", id)
    .withGraphFetched(
      "[pekerjaan, statusSuamiIstri, anak.[statusAnak,pekerjaanAnak ]]"
    );

  res.json({ data });
};
const detail = async (req, res) => {};
const update = async (req, res) => {};
const remove = async (req, res) => {};
const patch = async (req, res) => {};

module.exports = {
  index,
  detail,
  update,
  remove,
  patch,
};
