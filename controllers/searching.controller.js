const model = require("../models/ptt-biodata.model");

module.exports.search = async (req, res) => {
  try {
    const { employee_number } = req.params;
    const result = await model
      .query()
      .where("niptt", employee_number)
      .select("niptt", "id_ptt", "nama", "no_hp", "email");
    res.json(result);
  } catch (error) {
    res.boom.badImplementation("Internal Server Error");
  }
};
