const model = require("../models/ptt-pendidikan.model");

const index = async (req, res) => {
  const { id } = req.params;
  const result = await model
    .query()
    .where("id_ptt", id)
    .withGraphFetched("[jenjang]");
  res.json({ data: result });
};

const detail = async (req, res) => {
  const { pendidikanId, id } = req.params;
  const result = await model
    .query()
    .where("id_ptt", id)
    .andWhere("ptt_pendidikan_id", pendidikanId)
    .first();
  res.json({ data: result });
};

const create = async (req, res) => {};
const update = async (req, res) => {};
const remove = async (req, res) => {};

module.exports = {
  detail,
  index,
  create,
  update,
  remove,
};
