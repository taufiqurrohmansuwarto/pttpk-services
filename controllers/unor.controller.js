const unorModel = require("../models/skpd.model");
const employeeModel = require("../models/ptt-biodata.model");

const arrayToTree = require("array-to-tree");
const detailSkpd = require("../utils/detail-skpd");
const groupBy = require("lodash/groupBy");

// showing the unor
const detail = async (req, res) => {
  const { id } = req.params;
  const { query } = req;
  const result = await unorModel
    .query()
    .where("id", "like", `${id}%`)
    .select("id", "pId", "name");

  if (!result) {
    res.boom.badRequest("hello");
  } else {
    const { type } = query;
    if (type === "tree") {
      res.json({
        data: arrayToTree(result, { customID: "id", parentProperty: "pId" }),
      });
    }
    if (type === "flat") {
      res.json({ data: result });
    }
  }
};

// showing the employee who has unor id
// for fasilitator only and it should be cache
// this is why fucking reported so fucking slow in microservices
const getEmployee = async (req, res) => {
  const { id } = req.params;
  const result = await employeeModel
    .query()
    .where("id_skpd", "like", `${id}%`)
    .select("id_ptt", "niptt", "nama", "id_skpd")
    .andWhere("aktif", "Y")
    .andWhere("blokir", "N")
    .orderBy("niptt", "ASC");

  let data;
  if (result.length) {
    let promise = [];
    const uniqSkpd = detailSkpd.findUnique(result);
    uniqSkpd.forEach((x) => promise.push(detailSkpd.detail(x)));
    const hasil = await Promise.all(promise);
    data = result?.map((r) => {
      const detailSkpd = hasil.find((x) => x.id === r?.id_skpd);
      return {
        ...r,
        perangkat_daerah: detailSkpd?.detail,
      };
    });
  }

  res.json({ data });
};

module.exports = {
  getEmployee,
  detail,
};
