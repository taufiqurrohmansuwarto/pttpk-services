const express = require("express");
const router = express.Router({ mergeParams: true });

const searchingController = require("../../controllers/searching.controller");

router.route("/:employee_number").get(searchingController.search);

module.exports = router;
