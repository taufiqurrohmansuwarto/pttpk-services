const express = require("express");
const router = express.Router({ mergeParams: true });
const pendidikanController = require("../../controllers/pendidikan.controller");

router.route("/").get(pendidikanController.index);
router.route("/:pendidikanId").get();

module.exports = router;
