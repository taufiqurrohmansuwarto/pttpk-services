const express = require("express");
const router = express.Router({ mergeParams: true });
const userDepartmentController = require("../../../controllers/users/departments.controller");

router.route("/").get(userDepartmentController.list);

module.exports = router;
