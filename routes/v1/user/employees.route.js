const express = require("express");
const router = express.Router({ mergeParams: true });

const userEmployeesController = require("../../../controllers/users/employees.controller");

router.route("/").get(userEmployeesController.list);

module.exports = router;
