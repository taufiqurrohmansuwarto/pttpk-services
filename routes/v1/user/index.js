const express = require("express");
const {
  userActive,
} = require("../../../middlewares/is-active-person.middleware.js");
const router = express.Router({ mergeParams: true });

router.use(userActive);
router.use("/departments", require("./departments.route.js"));
router.use("/employees", require("./employees.route.js"));

module.exports = router;
