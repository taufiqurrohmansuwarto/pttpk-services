const express = require("express");
const router = express.Router({ mergeParams: true });
const keluargaController = require("../../controllers/keluarga.controller");
const {
  employeeActive,
} = require("../../middlewares/is-active-person.middleware");

router.route("/").get(employeeActive, keluargaController.index);

module.exports = router;
