const express = require("express");
const Joi = require("joi");
const router = express.Router();
const validator = require("express-joi-validation").createValidator({});
const unorController = require("../../controllers/unor.controller");

// validation
const schema = Joi.object({
  id: Joi.number().integer().required(),
});

// todo should be multiple values flat and tree
const querySchema = Joi.object({
  type: Joi.string().default("tree"),
});

router
  .route("/:id")
  .get(
    validator.params(schema),
    validator.query(querySchema),
    unorController.detail
  );

router.route("/:id/pegawai").get(unorController.getEmployee);

module.exports = router;
