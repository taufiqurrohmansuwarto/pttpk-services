const express = require("express");
const router = express.Router({ mergeParams: true });
const biodataController = require("../../controllers/biodata.controller");
const validator = require("express-joi-validation").createValidator({});
const employeeController = require("../../controllers/users/employees.controller");

// schema motherfucker
const { params } = require("../../validation/biodata.validation");

// biodata

router.use("/searching", require("./searching.route"));
router.route("/").get(employeeController.employee);

router.route("/:id").get(validator.params(params), biodataController.detail);

router.use("/:id/pendidikan", require("./pendidikan.route"));
router.use("/:id/jabatan", require("./jabatan.route"));
router.use("/:id/keluarga", require("./keluarga.route"));

module.exports = router;
