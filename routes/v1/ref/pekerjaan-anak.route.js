const express = require("express");
const router = express.Router();
const refPekerjaanAnakController = require("../../../controllers/ref-pekerjaan-anak.controller");

router.route("/").get(refPekerjaanAnakController.list);

module.exports = router;
