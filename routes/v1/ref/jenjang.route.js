const express = require("express");
const router = express.Router();
const refJenjangController = require("../../../controllers/ref-jenjang.controller");

router.route("/").get(refJenjangController.list);

module.exports = router;
