const express = require("express");
const router = express.Router();

router.use("/unor", require("./unor.routes"));
router.use("/pegawai", require("./pegawai.route"));
router.use("/ref", require("./ref"));

// fasilitator route
router.use("/fasilitator/:username", require("./user"));

module.exports = router;
