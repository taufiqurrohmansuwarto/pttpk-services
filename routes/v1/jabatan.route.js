const express = require("express");
const router = express.Router({ mergeParams: true });
const jabatanController = require("../../controllers/jabatan.controller");

router.route("/").get(jabatanController.index);
router.route("/:id").get(jabatanController.detail);

module.exports = router;
